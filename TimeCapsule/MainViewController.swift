//
//  MainViewController.swift
//  TimeCapsule
//
//  Created by Anoob Soman on 18/05/2018.
//  Copyright © 2018 Anoob Soman. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var teamName: UITextField!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var highScoresButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        playButton.layer.cornerRadius = 8
        playButton.layer.masksToBounds = true
        highScoresButton.layer.cornerRadius = 8
        highScoresButton.layer.masksToBounds = true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func playButtonTapped(_ sender: Any) {
        if teamName.text != "" {
            if let mapView = storyboard?.instantiateViewController(withIdentifier: "mapView") as? MapViewController {
                mapView.teamNameStr = self.teamName.text!
                self.present(mapView, animated: true, completion: nil)
            }
        } else {
            let alert = UIAlertController(title: "Team Name Required", message: "A team name is required before you start the challenges!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func settingsButtonTapped(_ sender: Any) {
        let alert = UIAlertController(title: "Enter Password", message: "", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Password"
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Enter", style: .default, handler: { action in
            if alert.textFields![0].text == "citrix123" {
                self.presentAdmin()
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func presentAdmin() {
        guard let splitView = storyboard?.instantiateViewController(withIdentifier: "splitView") as? AdminSplitViewController,
            let navView = splitView.viewControllers.first as? AdminNavigationController,
            let puzzlesView = navView.topViewController as? PuzzlesTableViewController,
            let detailView = splitView.viewControllers.last as? DetailTableViewController
            else { fatalError() }
        
        puzzlesView.delegate = detailView
        detailView.delegate = puzzlesView
        
        self.present(splitView, animated: true, completion: nil)
    }
    
    @IBAction func highScoreButtonTapped(_ sender: Any) {
        if let highScoresView = storyboard?.instantiateViewController(withIdentifier: "highScoresView") as? HighScoresViewController {
            
            //createFakeScores()
            
            highScoresView.providesPresentationContextTransitionStyle = true
            highScoresView.definesPresentationContext = true
            highScoresView.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            highScoresView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            
            self.present(highScoresView, animated: true, completion: nil)
        }
    }
    
    // dummy function for creating fake scores
    /*
    func createFakeScores() {
        let highScores = [HighScore(teamName: "Alpha", score: 500, time: 3072),
                          HighScore(teamName: "Beta", score: 430, time: 3600),
                          HighScore(teamName: "Charlie", score: 370, time: 3600),
                          HighScore(teamName: "Delta", score: 400, time: 3600),
                          HighScore(teamName: "Echo", score: 500, time: 2569),
                          HighScore(teamName: "Foxtrot", score: 290, time: 3600)]
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: highScores), forKey: "highScores")
    }*/
    
}
