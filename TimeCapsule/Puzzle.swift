//
//  Puzzle.swift
//  TimeCapsule
//
//  Created by Roddy Munro on 24/09/2018.
//  Copyright © 2018 Anoob Soman. All rights reserved.
//

import UIKit
import MapKit

@objc(Puzzle)
public class Puzzle : NSObject, NSCoding {
    public var index: Int!
    public var title: String!
    public var desc: String!
    public var coordinates: CLLocationCoordinate2D!
    public var locationDescription: String!
    public var code: String!
    public var points: Int!
    public var image: UIImage!
    public var status: String = "Not Visited"
    public var disabled: Bool = false
    
    public init (index: Int, title: String, desc: String, latitude: Double, longitude: Double,  locationDescription: String, code: String, points: Int) {
        self.index = index
        self.title = title
        self.desc = desc
        self.coordinates = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        self.locationDescription = locationDescription
        self.code = code
        self.points = points
        self.image = UIImage(named: "puzzle-\(index)")
    }
    
    func updateStatus(_ newStatus: String) {
        self.status = newStatus
    }
    
    func setDisabledStatus(_ disabled: Bool) {
        self.disabled = disabled
    }
    
    required public init(coder aDecoder: NSCoder) {
        self.index = aDecoder.decodeInteger(forKey: "index")
        
        if let title = aDecoder.decodeObject(forKey: "title") as? String {
            self.title = title
        }
        
        if let desc = aDecoder.decodeObject(forKey: "desc") as? String {
            self.desc = desc
        }
        
        if let locationDescription = aDecoder.decodeObject(forKey: "locationDescription") as? String {
            self.locationDescription = locationDescription
        }
        
        if let code = aDecoder.decodeObject(forKey: "code") as? String {
            self.code = code
        }
        
        self.points = aDecoder.decodeInteger(forKey: "points")
        
        if let image = aDecoder.decodeObject(forKey: "image") as? UIImage {
            self.image = image
        }
        
        let latitude = aDecoder.decodeDouble(forKey: "latitude")
        let longitude = aDecoder.decodeDouble(forKey: "longitude")
        self.coordinates = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        
        self.disabled = aDecoder.decodeBool(forKey: "disabled")
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(index!, forKey: "index")
        aCoder.encode(title, forKey: "title")
        aCoder.encode(desc, forKey: "desc")
        aCoder.encode(locationDescription, forKey: "locationDescription")
        aCoder.encode(code, forKey: "code")
        aCoder.encode(points!, forKey: "points")
        aCoder.encode(image, forKey: "image")
        aCoder.encode(coordinates.latitude, forKey: "latitude")
        aCoder.encode(coordinates.longitude, forKey: "longitude")
        aCoder.encode(disabled, forKey: "disabled")
    }
}
