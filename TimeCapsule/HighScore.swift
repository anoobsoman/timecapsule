//
//  HighScore.swift
//  TimeCapsule
//
//  Created by Roddy Munro on 24/09/2018.
//  Copyright © 2018 Anoob Soman. All rights reserved.
//

import UIKit

@objc(HighScore)
public class HighScore : NSObject, NSCoding {
    public var teamName: String!
    public var score: Int!
    public var time: Int!
    
    public init (teamName: String, score: Int, time: Int) {
        self.teamName = teamName
        self.score = score
        self.time = time
    }
    
    required public init(coder aDecoder: NSCoder) {
        if let teamName = aDecoder.decodeObject(forKey: "teamName") as? String {
            self.teamName = teamName
        }
        
        self.score = aDecoder.decodeInteger(forKey: "score")
        
        self.time = aDecoder.decodeInteger(forKey: "time")
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(teamName, forKey: "teamName")
        aCoder.encode(score!, forKey: "score")
        aCoder.encode(time!, forKey: "time")
    }
}
