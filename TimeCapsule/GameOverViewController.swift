//
//  GameOverViewController.swift
//  TimeCapsule
//
//  Created by Roddy Munro on 25/09/2018.
//  Copyright © 2018 Anoob Soman. All rights reserved.
//

import UIKit

class GameOverViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var closeButton: UIButton!
    
    var puzzles = [Puzzle]()
    var score: HighScore!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        saveNewScore()
        setUpView()
        animateView()
    }
    
    func saveNewScore() {
        var highScores = [HighScore]()
        
        if let data = UserDefaults.standard.object(forKey: "highScores") as? NSData {
            highScores = (NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? [HighScore])!
            highScores.append(score)
        } else {
            highScores = [score]
        }
        
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: highScores), forKey: "highScores")
    }
    
    func animateView() {
        self.popUpView.alpha = 0;
        self.popUpView.frame.origin.y = self.popUpView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.popUpView.alpha = 1.0;
            self.popUpView.frame.origin.y = self.popUpView.frame.origin.y - 50
        })
    }
    
    func setUpView() {
        self.popUpView.layer.cornerRadius = 8
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
        self.closeButton.layer.cornerRadius = 8
        self.closeButton.layer.masksToBounds = true
        
        if score.time >= 20 { //3600 {
            messageLabel.text = "Game Over! You ran out of time!"
        } else {
            messageLabel.text = "Congratulations!"
        }
        
        scoreLabel.text = "Score: \((score.score)!)"
        timeLabel.text = "Time: \(String(format: "%02d:%02d", score.time/60, score.time%60))"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return puzzles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "puzzleCell")
        
        let puzzle = puzzles[indexPath.row]
        
        switch(puzzle.status) {
        case "Not Visited", "In Progress":
            cell?.textLabel?.textColor = .red
            cell?.textLabel!.text = "\((puzzle.index)!) - \((puzzle.title)!) - Incomplete"
            break
            
        case "Completed":
            cell?.textLabel?.textColor = UIColor(red: 0, green: 153/255, blue: 0, alpha: 1.0)
            cell?.textLabel!.text = "\((puzzle.index)!) - \((puzzle.title)!) - Completed"
            break
            
        default:
            cell?.textLabel?.textColor = .black
            break
        }
        
        return cell!
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
    }
}
