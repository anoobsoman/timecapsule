//
//  PopUpViewDelegate.swift
//  TimeCapsule
//
//  Created by Roddy Munro on 20/09/2018.
//  Copyright © 2018 Anoob Soman. All rights reserved.
//

protocol PopUpViewDelegate: class {
    func codeCorrect(_ scoreToAdd: Int, _ chosenPuzzleIndex: Int)
}
