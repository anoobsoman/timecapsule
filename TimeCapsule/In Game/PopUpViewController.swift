//
//  PopUpViewController.swift
//  TimeCapsule
//
//  Created by Roddy Munro on 20/09/2018.
//  Copyright © 2018 Anoob Soman. All rights reserved.
//

import UIKit

class PopUpViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var puzzleNo: UILabel!
    @IBOutlet weak var puzzleTitle: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var puzzleDescription: UILabel!
    @IBOutlet weak var puzzleImage: UIImageView!
    
    @IBOutlet weak var puzzleCodeText: UITextView!
    
    @IBOutlet weak var enterButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    
    var delegate: PopUpViewDelegate?
    
    var chosenPuzzle: Puzzle!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        puzzleCodeText.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpView()
        animateView()
    }
    
    func animateView() {
        self.popUpView.alpha = 0;
        self.popUpView.frame.origin.y = self.popUpView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.popUpView.alpha = 1.0;
            self.popUpView.frame.origin.y = self.popUpView.frame.origin.y - 50
        })
    }
    
    func setUpView() {
        self.popUpView.layer.cornerRadius = 8
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
        self.puzzleNo.text = "Puzzle-\((chosenPuzzle.index)!)"
        self.puzzleTitle.text = chosenPuzzle.title
        self.pointsLabel.text = String(chosenPuzzle.points)
        self.puzzleDescription.text = chosenPuzzle.desc
        
        let image_name = "puzzle-\((chosenPuzzle.index)!)"
        if let image = UIImage(named:image_name) {
            self.puzzleImage.image = image
        }
        
        puzzleCodeText.text = "Enter Code Here"
        puzzleCodeText.textColor = .lightGray
        puzzleCodeText.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).cgColor
        puzzleCodeText.layer.borderWidth = 0.5
        puzzleCodeText.layer.cornerRadius = 5
        
        self.statusLabel.text = chosenPuzzle.status
        
        switch(chosenPuzzle.status) {
        case "Not Visited":
            self.puzzleCodeText.isUserInteractionEnabled = false
            self.enterButton.isEnabled = false
            self.statusLabel.textColor = .red
            break
            
        case "In Progress":
            self.puzzleCodeText.isUserInteractionEnabled = true
            self.enterButton.isEnabled = true
            self.statusLabel.textColor = .orange
            break
            
        case "Completed":
            self.puzzleCodeText.isUserInteractionEnabled = false
            self.enterButton.isEnabled = false
            self.statusLabel.textColor = UIColor(red: 0, green: 153/255, blue: 0, alpha: 1.0)
            break
            
        default:
            self.puzzleCodeText.isUserInteractionEnabled = false
            self.statusLabel.textColor = .black
            break
        }
        
        self.closeButton.layer.cornerRadius = 8
        self.closeButton.layer.masksToBounds = true
        
        self.enterButton.layer.cornerRadius = 8
        self.enterButton.layer.masksToBounds = true
    }
    
    @IBAction func enterButtonTapped(_ sender: Any) {
        if puzzleCodeText.text != "" && chosenPuzzle.code == puzzleCodeText.text {
            let alert = UIAlertController(title: "Correct!", message: "Congratulations. You completed Puzzle \((chosenPuzzle.index)!)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                self.delegate?.codeCorrect(self.chosenPuzzle.points, self.chosenPuzzle.index)
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        } else if puzzleCodeText.text == "" {
            let alert = UIAlertController(title: "Code Not Entered", message: "You didn't enter a code. Please enter a code!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Incorrect", message: "The code you entered is incorrect!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == .lightGray {
            textView.text = nil
            textView.textColor = .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Enter Code Here"
            textView.textColor = .lightGray
        }
    }
}
