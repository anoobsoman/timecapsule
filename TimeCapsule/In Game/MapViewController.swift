//
//  MapViewController.swift
//  TimeCapsule
//
//  Created by Anoob Soman on 19/07/2018.
//  Copyright © 2018 Anoob Soman. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import MobileCoreServices

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var teamNameDisplay: UILabel!
    @IBOutlet weak var puzzleHM: UILabel!
    @IBOutlet weak var puzzleSecond: UILabel!
    
    var locationManager = CLLocationManager()
    var updateMapView: Bool = true
    var userLocationCoord = CLLocationCoordinate2D()
    
    var puzzles = [Puzzle]()
    var score: Int = 0
    var teamNameStr: String = ""
    var timer: Timer?
    var counter = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData()

        let authStatus = CLLocationManager.authorizationStatus()
        mapView.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(sender:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(sender:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self

        setupUserTrackingButton()

        if authStatus == .denied || authStatus == .restricted {
            print("location service disabled")
            /* TODO, what should be done here, if location service is disabled */
            /* Should we continue with the application or bail out */
        }

        //Check for Location Services
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
        }
        
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        
        DispatchQueue.main.async {
            self.locationManager.startUpdatingLocation()
        }

        scoreLabel.text = String(score) + " POINTS"
        teamNameDisplay.text = teamNameStr
        
        timer = Timer.scheduledTimer(timeInterval:1, target:self, selector:#selector(processTimer), userInfo: nil, repeats: true)

    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -150 // Move view 150 points upward
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0 // Move view to original position
    }
    
    func setupUserTrackingButton() {
        let button = MKUserTrackingButton(mapView: mapView)
        button.layer.backgroundColor = UIColor(white: 1, alpha: 0.8).cgColor
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        button.frame = CGRect(x: mapView.bounds.maxX - 50, y: 0, width: 50, height: 50)
        button.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]
//        button.center = CGPoint(x: mapView.frame.width, y: mapView.frame.height/2)
        mapView.addSubview(button)
    }
    
    func loadData() {
        if let data = UserDefaults.standard.object(forKey: "puzzles") as? NSData {
            self.puzzles = (NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? [Puzzle])!
        } else {
            self.addFirstTimeData()
        }
        
        plotPoints()
    }
    
    func plotPoints() {
        mapView.removeAnnotations(mapView.annotations)
        
        var count = 0
        for puzzle in puzzles {
            if puzzle.disabled {
                self.puzzles.remove(at: count)
            } else {
                let annotation = MKPointAnnotation()
                
                annotation.coordinate = puzzle.coordinates
                annotation.title = puzzle.title
                annotation.subtitle = ""
                
                mapView.addAnnotation(annotation)
                count += 1
            }
        }
    }
    
    func addFirstTimeData() {
        self.puzzles = [
            Puzzle(index: 1, title: "Jurassic Era", desc: "A mere 66 million years ago, a large object hit the developing planet Earth, creating a huge dust storm that lasted 100s of years. Light levels dropped, and the dinosaurs became extinct. Look closely at the image, open the locks, and find the dinosaur antique locked away.", latitude: 52.23213, longitude: 0.14939, locationDescription: "By bench near 101 building", code: "HH770X", points: 230),
            Puzzle(index: 2, title: "Near Future 2027", desc: "It’s 2027 and geolocation devices have become vital to society. Every object can be tracked within a few cm at all times. This strange futuristic object seems to change its display depending where it is. Maybe it needs to be somewhere soon?", latitude: 52.23218, longitude: 0.14615, locationDescription: "By main Trinity Centre entrance bench. Reverse Geocache", code: "WWPL45X", points: 280),
            Puzzle(index: 3, title: "Far Future Maze Cryptex", desc: "It’s the year 4198. Multiple attempts to come back to Earth after the INCIDENT have failed. Organic matter has gone.  It seems the key to everything is the code inside this unusual device made from artificial wood. Can you open it? Red Box LX2 to 40. R to 4", latitude: 52.23348, longitude: 0.14691, locationDescription: "In front of Bradfield Centre - cryptex", code: "AXXPANX", points: 150),
            Puzzle(index: 4, title: "1991 Russian Box", desc: "I will never forgive the West. The glory of the Soviet motherland will remain, always. These rare Matryoshka dolls hide a code of some importance. A dark metal box contains my secrets. Tread carefully, comrade, they are WATCHING YOU.", latitude: 52.23393, longitude: 0.14521, locationDescription: "By lake back of Bradfield", code: "PLK48Q", points: 110),
            Puzzle(index: 5, title: "1974 - Cold War", desc: "A tragic story from the mid 1970s lives inside this box. I can feel it, almost smell the misery within. I just need that code. The cold war changed the path of humanity. Something feels different about this military box versus the photograph I used to source this case. But how?", latitude: 52.23416, longitude: 0.14297, locationDescription: "Bridge over lake", code: "GZFSWPZ", points: 220),
            Puzzle(index: 6, title: "1717 - Indemnity Act", desc: "TBD", latitude: 52.23493, longitude: 0.14784, locationDescription: "Bridge over lake", code: "KKYBV88", points: 100),
            Puzzle(index: 7, title: "2013 Modern Day Emoji box", desc: "This antique box from the 2010s has a strange set of symbols known then as emoji. Little did the people of that time know that they would transform into the entire global language by 2095. Of course, the destruction of all electronics shortly after meant only the written word in archived books survived and back to English we went. ;-)", latitude: 52.23189, longitude: 0.14495, locationDescription: "Bridge over lake", code: "PPX74YZ", points: 175),
            Puzzle(index: 8, title: "1867 Middle Eastern", desc: "It was 1867 and the visitor was tired beyond anything he’d ever known. The arabic rune which was so precious to his family was locked away, somewhere in the desert land. An arabic script was all he had, 12 miles from the gates, and suddenly it all made sense...", latitude: 52.23011, longitude: 0.14859, locationDescription: "Bridge over lake", code: "JRXMXA8", points: 290),
            Puzzle(index: 9, title: "2019 - Free to hate", desc: "Before Social Media War I, the planet went through a huge period of online hatred. Vile arguments on every conceivable topic from politics to pop stars polluted the early internet. This box was interesting to me as it represented the start of locking up vile “trolls” to use the parlance of the day.", latitude: 52.23622, longitude: 0.14267, locationDescription: "Bridge over lake", code: "TBD", points: 95),
            Puzzle(index: 11, title: "Pirate Magic box", desc: "Alas, the treasure was no more. A barrel of rum, some gold coins stamped 1590 and a terrible tale of voodoo magic was all that was left. The story song told over many a flagon of grog went something like this:..the treasure chest was behest / to magic of voodoo style / to open the box / no locks but symbols that match / just wait a while...", latitude: 52.23462, longitude: 0.14685, locationDescription: "Bridge over lake", code: "TBD", points: 95),
            Puzzle(index: 12, title: "1079 Spy hole box", desc: "A strange time, the 1070s. Many strange artifacts arrived, especially in the year 1066 which the strange light in the sky. No one could understand this apparition, and its purpose. One man hid secrets only by the light of the comet itself. It seemed obvious I should take this box, but how to read it secret?", latitude: 52.2336, longitude: 0.14567, locationDescription: "Bridge over lake", code: "KLATJXP", points: 140)
        ]
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: self.puzzles), forKey: "puzzles")
    }
    
    @objc func processTimer() {
        counter += 1
        if !checkGameOver() {
            puzzleHM.text = String(format: "%02d:%02d", 00, 59 - counter/60)
            puzzleSecond.text = String(format: ":%02d", 60 - counter%60)
        } else {
            self.presentedViewController?.dismiss(animated: true, completion: nil)
            timer?.invalidate()
            locationManager.stopUpdatingLocation()
            
            // end game, move to new view
            let newScore = HighScore(teamName: self.teamNameStr, score: self.score, time: self.counter)
            
            if let gameOverView = storyboard?.instantiateViewController(withIdentifier: "gameOverView") as? GameOverViewController {
                gameOverView.puzzles = self.puzzles
                gameOverView.score = newScore
                
                gameOverView.providesPresentationContextTransitionStyle = true
                gameOverView.definesPresentationContext = true
                gameOverView.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                gameOverView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                
                self.present(gameOverView, animated: true, completion: nil)
            }
        }
    }
    
    func checkGameOver() -> Bool {
        if counter >= 3600 {
            return true
        } else {
            var puzzlesComplete = true
            
            for puzzle in puzzles {
                if puzzle.status != "Completed" {
                    puzzlesComplete = false
                    break
                }
            }
            return puzzlesComplete
        }
    }
    
    func showPopUpView(_ title: String) {
        for puzzle in puzzles {
            if puzzle.title == title {
                if let popUpView = storyboard?.instantiateViewController(withIdentifier: "popUpView") as? PopUpViewController {
                    popUpView.chosenPuzzle = puzzle
                    
                    popUpView.providesPresentationContextTransitionStyle = true
                    popUpView.definesPresentationContext = true
                    popUpView.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    popUpView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    popUpView.delegate = self
                    
                    self.present(popUpView, animated: true, completion: nil)
                }
                break
            }
        }
    }
}

// MARK: - MKMapViewDelegate

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if !(view.annotation is MKUserLocation) {
            if let viewTitle = (view.annotation?.title) {
                showPopUpView(viewTitle!)
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        let reuseId = "puzzle"
        
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
        if pinView == nil {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
        } else {
            pinView!.annotation = annotation
        }
        
        for puzzle in puzzles {
            if puzzle.title == annotation.title {
                switch(puzzle.status) {
                case "Not Visited":
                    pinView?.pinTintColor = .red
                    break
                    
                case "In Progress":
                    pinView?.pinTintColor = .orange
                    break
                    
                case "Completed":
                    pinView?.pinTintColor = .green
                    break
                    
                default:
                    pinView?.pinTintColor = .red
                    break
                }
                break
            }
        }
        
        return pinView
    }
}

// MARK: - CLLocationManagerDelegate

extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        //manager.stopUpdatingLocation()
        
        userLocationCoord = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        let region = MKCoordinateRegion(center: userLocationCoord, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        if updateMapView {
            mapView.setRegion(region, animated: true)
            updateMapView = false
        }
        
        for puzzle in puzzles {
            let pinLoc = CLLocation(latitude: puzzle.coordinates.latitude, longitude: puzzle.coordinates.longitude)

            if userLocation.distance(from: pinLoc) < 25 && puzzle.status == "Not Visited" {
                puzzle.updateStatus("In Progress")
                showPopUpView(puzzle.title)
            }
        }
    }
}

// MARK: - PopUpViewDelegate

extension MapViewController: PopUpViewDelegate {
    func codeCorrect(_ scoreToAdd: Int, _ chosenPuzzleIndex: Int) {
        score += scoreToAdd
        scoreLabel.text = "\(score)"
        
        for puzzle in puzzles {
            if puzzle.index == chosenPuzzleIndex {
                puzzle.updateStatus("Completed")
                plotPoints()
                break
            }
        }
    }
}
