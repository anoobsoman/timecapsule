//
//  HighScoresViewController.swift
//  TimeCapsule
//
//  Created by Roddy Munro on 25/09/2018.
//  Copyright © 2018 Anoob Soman. All rights reserved.
//

import UIKit

class HighScoresViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var closeButton: UIButton!
    
    var highScores = [HighScore]()
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpView()
        animateView()
        loadData()
    }
    
    func loadData() {
        if let data = UserDefaults.standard.object(forKey: "highScores") as? NSData {
            self.highScores = (NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? [HighScore])!
            self.highScores = self.highScores.sorted(by: { (s1, s2) -> Bool in
                if s1.score == s2.score {
                    return s1.time < s2.time
                } else {
                    return s1.score > s2.score
                }
            })
            self.tableView.reloadData()
        }
    }
    
    func animateView() {
        self.popUpView.alpha = 0;
        self.popUpView.frame.origin.y = self.popUpView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.popUpView.alpha = 1.0;
            self.popUpView.frame.origin.y = self.popUpView.frame.origin.y - 50
        })
    }
    
    func setUpView() {
        self.popUpView.layer.cornerRadius = 8
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
        self.closeButton.layer.cornerRadius = 8
        self.closeButton.layer.masksToBounds = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return highScores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "highScoreCell")
        
        let score = highScores[indexPath.row]
        
        cell?.textLabel!.text = "\(indexPath.row + 1) - \((score.teamName)!) - \((score.score)!) - \(String(format: "%02d:%02d", score.time/60, score.time%60))"
        
        return cell!
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
