//
//  DetailTableViewController.swift
//  TimeCapsule
//
//  Created by Roddy Munro on 24/09/2018.
//  Copyright © 2018 Anoob Soman. All rights reserved.
//

import UIKit
import MapKit

class DetailTableViewController: UITableViewController, UISplitViewControllerDelegate {
    
    var chosenPuzzle: Puzzle?
    var addMode: Bool = false
    
    @IBOutlet var labels: [UILabel]!
    
    @IBOutlet weak var disabledSwitch: UISwitch!
    @IBOutlet weak var indexTextField: UITextField!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var latitudeTextField: UITextField!
    @IBOutlet weak var longitudeTextField: UITextField!
    @IBOutlet weak var locationDescriptionTextField: UITextField!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var pointsTextField: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var discardAddButton: UIButton!
    @IBOutlet weak var discardEditButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    
    
    var imagePicker = UIImagePickerController()
    
    weak var delegate: PuzzleUpdatedDelegate?
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateData()
        
        deleteButton.layer.cornerRadius = 8
        deleteButton.layer.masksToBounds = true
        discardEditButton.layer.cornerRadius = 8
        discardEditButton.layer.masksToBounds = true
        saveButton.layer.cornerRadius = 8
        saveButton.layer.masksToBounds = true
        
        addButton.layer.cornerRadius = 8
        addButton.layer.masksToBounds = true
        discardAddButton.layer.cornerRadius = 8
        discardAddButton.layer.masksToBounds = true

        self.splitViewController!.preferredDisplayMode = UISplitViewControllerDisplayMode.allVisible
    }
    
    func updateData() {
        if chosenPuzzle != nil {
            disabledSwitch.isOn = (chosenPuzzle?.disabled)!
            indexTextField.text = String((chosenPuzzle?.index)!)
            titleTextField.text = chosenPuzzle?.title
            descriptionTextView.text = chosenPuzzle?.desc
            latitudeTextField.text = String((chosenPuzzle?.coordinates.latitude)!)
            longitudeTextField.text = String((chosenPuzzle?.coordinates.longitude)!)
            locationDescriptionTextField.text = chosenPuzzle?.locationDescription
            codeTextField.text = chosenPuzzle?.code
            pointsTextField.text = String((chosenPuzzle?.points)!)
            imageView.image = chosenPuzzle?.image
            deleteButton.isEnabled = true
            discardEditButton.isEnabled = true
            saveButton.isEnabled = true
            addButton.isEnabled = false
            discardAddButton.isEnabled = false
        } else if addMode {
            disabledSwitch.isOn = false
            indexTextField.text = ""
            titleTextField.text = ""
            descriptionTextView.text = ""
            latitudeTextField.text = ""
            longitudeTextField.text = ""
            locationDescriptionTextField.text = ""
            codeTextField.text = ""
            pointsTextField.text = ""
            imageView.image = nil
            deleteButton.isEnabled = false
            discardEditButton.isEnabled = false
            saveButton.isEnabled = false
            addButton.isEnabled = true
            discardAddButton.isEnabled = true
        } else {
            disabledSwitch.isOn = false
            indexTextField.text = ""
            titleTextField.text = ""
            descriptionTextView.text = ""
            latitudeTextField.text = ""
            longitudeTextField.text = ""
            locationDescriptionTextField.text = ""
            codeTextField.text = ""
            pointsTextField.text = ""
            imageView.image = nil
            deleteButton.isEnabled = false
            discardEditButton.isEnabled = false
            saveButton.isEnabled = false
            addButton.isEnabled = false
            discardAddButton.isEnabled = false
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 10 && (chosenPuzzle != nil || addMode) {
            chooseImage()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 78
        } else if indexPath.row == 4 {
            return 121
        } else if indexPath.row == 10 {
            return 161
        } else if indexPath.row == 11 {
            if addMode {
                return 0
            } else {
                return 60
            }
        } else if indexPath.row == 12 {
            if addMode {
                return 60
            } else {
                return 0
            }
        } else {
            return 44
        }
    }
    
    @IBAction func deleteButtonTapped(_ sender: Any) {
        let alert = UIAlertController(title: "Deleting Puzzle", message: "Are you sure you want to delete this puzzle?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { action in
            self.deletePuzzle()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func deletePuzzle() {
        if let data = UserDefaults.standard.object(forKey: "puzzles") as? NSData {
            var puzzles = (NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? [Puzzle])!
            
            var count = 0
            for puzzle in puzzles {
                if chosenPuzzle?.index == puzzle.index {
                    puzzles.remove(at: count)
                    break
                }
                count += 1
            }
            
            UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: puzzles), forKey: "puzzles")
            self.delegate?.updatePuzzles()
        }
        
        self.chosenPuzzle = nil
        updateData()
    }
    
    @IBAction func discardButtonTapped(_ sender: Any) {
        let alert = UIAlertController(title: "Discarding Unsaved Changes", message: "Are you sure you want to discard all unsaved changes?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { action in
            self.updateData()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        if validateForm() {
            if let data = UserDefaults.standard.object(forKey: "puzzles") as? NSData {
                var puzzles = (NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? [Puzzle])!
                
                var count = 0
                for puzzle in puzzles {
                    if chosenPuzzle?.index == puzzle.index {
                        puzzles[count].index = Int(self.indexTextField.text!)
                        puzzles[count].title = self.titleTextField.text!
                        puzzles[count].desc = self.descriptionTextView.text
                        puzzles[count].coordinates = CLLocationCoordinate2D(latitude: Double(self.latitudeTextField.text!)!, longitude: Double(self.longitudeTextField.text!)!)
                        puzzles[count].locationDescription = self.locationDescriptionTextField.text!
                        puzzles[count].code = self.codeTextField.text!
                        puzzles[count].points = Int(self.pointsTextField.text!)
                        puzzles[count].image = self.imageView.image
                        puzzles[count].setDisabledStatus(self.disabledSwitch.isOn)
                        break
                    }
                    count += 1
                }
                
                UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: puzzles), forKey: "puzzles")
                self.delegate?.updatePuzzles()
            }
        } else {
            let alert = UIAlertController(title: "Missing Data", message: "Not all fields are full", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func addButtonTapped(_ sender: Any) {
        if validateForm() {
            if let data = UserDefaults.standard.object(forKey: "puzzles") as? NSData {
                var puzzles = (NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? [Puzzle])!
                
                let newIndex = Int(self.indexTextField.text!)
                let newTitle = self.titleTextField.text!
                let newDesc = self.descriptionTextView.text
                let newLatitude = Double(self.latitudeTextField.text!)
                let newLongitude = Double(self.longitudeTextField.text!)
                let newLocationDescription = self.locationDescriptionTextField.text!
                let newCode = self.codeTextField.text!
                let newPoints = Int(self.pointsTextField.text!)
                
                let newPuzzle = Puzzle(index: newIndex!, title: newTitle, desc: newDesc!, latitude: newLatitude!, longitude: newLongitude!, locationDescription: newLocationDescription, code: newCode, points: newPoints!)
                
                newPuzzle.image = self.imageView.image
                newPuzzle.setDisabledStatus(self.disabledSwitch.isOn)
                
                puzzles.append(newPuzzle)
                
                UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: puzzles), forKey: "puzzles")
                self.delegate?.updatePuzzles()
                self.updateData()
            }
        } else {
            let alert = UIAlertController(title: "Missing Data", message: "Not all fields are full", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func validateForm() -> Bool {
        if indexTextField.text == "" {
            return false
        } else if titleTextField.text == "" {
            return false
        } else if descriptionTextView.text == "" {
            return false
        } else if latitudeTextField.text == "" {
            return false
        } else if longitudeTextField.text == "" {
            return false
        } else if locationDescriptionTextField.text == "" {
            return false
        } else if codeTextField.text == "" {
            return false
        } else if pointsTextField.text == "" {
            return false
        } else if imageView.image == nil {
            return false
        } else {
            return true
        }
    }
}

extension DetailTableViewController: PuzzleSelectionDelegate {
    func puzzleSelected(_ puzzle: Puzzle) {
        chosenPuzzle = puzzle
        addMode = false
        self.updateData()
        self.tableView.reloadData()
    }
    
    func showAddForm() {
        chosenPuzzle = nil
        addMode = true
        self.updateData()
        self.tableView.reloadData()
    }
}

extension DetailTableViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func chooseImage() {
        self.imagePicker.allowsEditing = false
        self.imagePicker.delegate = self
        self.imagePicker.sourceType = .photoLibrary
        
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imageView.contentMode = .scaleAspectFit
            self.imageView.image = chosenImage
        }
        dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
