//
//  PuzzleUpdatedDelegate.swift
//  TimeCapsule
//
//  Created by Roddy Munro on 24/09/2018.
//  Copyright © 2018 Anoob Soman. All rights reserved.
//

protocol PuzzleUpdatedDelegate: class {
    func updatePuzzles()
}
